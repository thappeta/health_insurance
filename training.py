from logging import Logger

import aws_to_local
from App_Logger.Logger import App_Logger
from CSV_loader import Data_loader


class trainModel:

    def __init__(self):
        self.log_writer = App_Logger()
        self.file_object = open("Logs_folder/dataset_logs.txt", 'a+')
    def trainingModel(self):
        # Logging the start of Training
        self.log_writer.log(self.file_object, 'Start of Trainings')
        try:

            aws_data = aws_to_local.AWS_Getter(self.file_object, self.log_writer)
            aws_data.S3_data_fetch()

            # Getting the data from the source
            data_getter=Data_loader.Data_Getter(self.file_object,self.log_writer)
            data=data_getter.get_data()
            return data


        except:
            print("error occured here")
