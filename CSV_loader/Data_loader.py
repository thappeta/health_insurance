import pandas as pd


class Data_Getter:

    def __init__(self, file_object, logger_object):
        self.training_file = 'MongoDb_CSV/HealthPrem.csv'
        self.file_object = file_object
        self.logger_object = logger_object

    def get_data(self):

        self.logger_object.log(self.file_object, 'Entered the get_data method Data_Getter')
        try:
            self.data = pd.read_csv(self.training_file)  # reading the data file
            self.logger_object.log(self.file_object, 'Data Load Successful.Exited the get_data method of the '
                                                     'Data_Getter class')
            return self.data.head()
        except Exception as e:
            self.logger_object.log(self.file_object, 'Exception occurred in get_data method of the Data_Getter class. '
                                                     'Exception message: ' + str(e))
            self.logger_object.log(self.file_object,
                                   'Data Load Unsuccessful.Exited the get_data method of the Data_Getter classs')
            raise Exception()
